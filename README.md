# OpenML dataset: Internet-Movie-Database

https://www.openml.org/d/43572

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
There's a story behind every dataset and here's your opportunity to share yours.
Imdb-data is dataset for various movies gathered together 
Content
What's inside is more than just rows and columns. Make it easy for others to get started by describing how you acquired the data and what time period it represents, too.
Imdb-data is dataset for various movies gathered together which contains Movie Release year, description, Revenue etc.
Acknowledgements
We wouldn't be here without the help of others. If you owe any attributions or thanks, include them here along with any citations of past research.
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43572) of an [OpenML dataset](https://www.openml.org/d/43572). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43572/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43572/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43572/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

